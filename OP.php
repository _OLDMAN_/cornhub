
<?php
    include("MySQL.php");

    if(isset($_GET['AddVideo']))
    {
        $sqlAdd = "INSERT INTO `video`(`名稱`, `價格`, `片商`, `類別`, `新增時間`, `瀏覽次數`, `會員限定`, `影片`, `圖片`) 
        VALUES ('{$_POST['videoName']}','{$_POST['price']}','{$_POST['videoProducer']}','{$_POST['tag']}','{$_POST['addTime']}','{$_POST['watchCount']}','{$_POST['member']}','{$_POST['videoHref']}','{$_POST['imgHref']}')";
        mysqli_query($link, $sqlAdd);
    }
    if(isset($_GET['DelVideoIndex']))
    {
        $sqlDel = "DELETE FROM `video` WHERE 編號 = {$_GET['DelVideoIndex']}";
        mysqli_query($link, $sqlDel);
    }
    if(isset($_GET['VideoIndex']))
    {
        $sqlEdit = "UPDATE `video` SET `名稱`='{$_POST['videoName']}',`價格`='{$_POST['price']}',`片商`='{$_POST['videoProducer']}',`類別`='{$_POST['tag']}',`新增時間`='{$_POST['addTime']}',`瀏覽次數`='{$_POST['watchCount']}',`會員限定`='{$_POST['member']}',`影片`='{$_POST['videoHref']}',`圖片`='{$_POST['imgHref']}'
                 WHERE 編號 = {$_GET['VideoIndex']}";
        mysqli_query($link, $sqlEdit);
    }
    $sql = "SELECT * FROM video";
    $videos = Query($link, $sql);
    $videoCount = count($videos);
    $result = mysqli_query($link, $sql);

    session_start();
    $memberSql = "SELECT * FROM member WHERE 編號 = {$_SESSION['UserNum']}";
    $result = mysqli_query($link, $memberSql);
    $member = mysqli_fetch_array($result);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="OP.css">
<!--The following script tag downloads a font from the Adobe Edge Web Fonts server for use within the web page. We recommend that you do not modify it.--><script>var __adobewebfontsappname__="dreamweaver"</script><script src="http://use.edgefonts.net/bilbo-swash-caps:n4:default;cabin-sketch:n7:default;cuprum:n4:default.js" type="text/javascript"></script>
</head>
<body id="backgroundSetting" background="img/backgroundFinal - 複製.png">
    <div class="TopFloat">
        <div class="TopFloat-Item01"><a href="Menu.php">CORNHUB</a></div>
        <div class="TopFloat-Item02">
            <form method="POST" action="SearchResult.php">
                <input type="text" name="search" id="SearchText" placeholder="搜尋影片">
                <input type="submit" id="SearchBtn" value="⊙搜尋>">
            </form>
        </div>
        <div class="TopFloat-Item04">
            <div>
                <?php
                    echo "<span id='UserName'>使用者:</span>";
                    echo "<span id='UserName'>{$member['名稱']}</span><br>";
                    echo "<span id='UserName'>會員狀態:</span>";
                    echo "<span id='UserName'>管理者</span>";
                ?>
            </div>
        </div>
        <div class="TopFloat-Item03">
            <form action="Add.php" class="">
                <input type="submit" id="UpgradeBtn" value=" ◹ 上傳">
            </form>
        </div>
    </div>

    <div class="VideoBlock">
        <span id="Title">影片清單</span>
        <div class="VideoList">
            <table>
                <?php
                    $row = 4;
                    $col = $videoCount / $row;
                    if($videoCount % $row != 0)
                        $col++;
                    for($j = 0; $j < $col; $j++)
                    {
                        echo "<tr>";
                        for ($i = 0; $i < $row; $i++) {
                            $count = $j * $row + $i;
                            if($count >= $videoCount)
                            break;
                            echo"<td>
                                <div class='Video'>
                                    <div class='VideoMp4'>
                                        <img id='VideoImg' src='{$videos[$count]['圖片']}'>
                                    </div>
                                    <div class='VideoInformation'>
                                        <div id='VideoInformationBarSetting'><label id='InformatioText'>影片名稱:{$videos[$count]['名稱']}</label><br>
                                        <label id='InformatioText'>價格:{$videos[$count]['價格']}</label><br>
                                        <label id='InformatioText'>新增日期:{$videos[$count]['新增時間']}</label><br>
                                        <label id='InformatioText'>瀏覽次數:{$videos[$count]['瀏覽次數']}</label><br>
                                        <label id='InformatioText'>發行商:{$videos[$count]['片商']}</label><br>
                                        <label id='InformatioText'>類別:{$videos[$count]['類別']}</label><br>
                                        <label id='InformatioText'>會員限定:{$videos[$count]['會員限定']}</label></div> 
                                        <div id='InteractiveButton'>
										<label id='InformatioBtn'><a class='Sp' href='Edit.php?videoIndex={$videos[$count]['編號']}'>修改</label>
                                        <label id='InformatioBtn'><a class='Sp' href='OP.php?DelVideoIndex={$videos[$count]['編號']}'>刪除</label>
										</div>
                                    </div>
                                </div>
                            </td>";
                        }
                        echo "</tr>";
                    }
                ?>
                
            </table>
        </div>
    </div>
</body>
</html>