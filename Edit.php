<?php
    include("MySQL.php");

    $sql = "SELECT * FROM video WHERE 編號 = {$_GET['videoIndex']}";
    $result = mysqli_query($link, $sql);
    $video = mysqli_fetch_array($result);

    session_start();
    $memberSql = "SELECT * FROM member WHERE 編號 = {$_SESSION['UserNum']}";
    $result = mysqli_query($link, $memberSql);
    $member = mysqli_fetch_array($result);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="Edit.css">
<!--The following script tag downloads a font from the Adobe Edge Web Fonts server for use within the web page. We recommend that you do not modify it.--><script>var __adobewebfontsappname__="dreamweaver"</script><script src="http://use.edgefonts.net/bowlby-one-sc:n4:default;cuprum:n4:default.js" type="text/javascript"></script>
</head>
<body id="backgroundSetting" background="img/backgroundFinal - 複製.png">
    <div class="TopFloat">
        <div class="TopFloat-Item01"><a href="Menu.php">CORNHUB</a></div>
        <div class="TopFloat-Item02">
            <form method="POST" action="SearchResult.php">
                <input type="text" name="search" id="SearchText" placeholder="搜尋影片">
                <input type="submit" id="SearchBtn" value="⊙搜尋>">
            </form>
        </div>
        <div class="TopFloat-Item04">
            <div>
                <?php
                    echo "<span id='UserName'>使用者:</span>";
                    echo "<span id='UserName'>{$member['名稱']}</span><br>";
                    echo "<span id='UserName'>會員狀態:</span>";
                    echo "<span id='UserName'>管理者</span>";
                ?>
            </div>
        </div>
        <div class="TopFloat-Item03">
            <form action="Add.php" class="">
                <input type="submit" id="UpgradeBtn" value=" ◹ 上傳">
            </form>
        </div>
    </div>

    <div class="VideoBlock">
        <span id="Title">修改影片</span>
        <div class="VideoList">
                <?php
                    echo "<div class='Video'>
                            <div class='VideoMp4'>
                                <img id='VideoImg' src='{$video['圖片']}'>
                            </div>
                            <form class='VideoInformation'>
                                <label id='InformatioText'>影片名稱:</label><input id='InformationInput' name='videoName' type='text' value='{$video['名稱']}'><br>
                                <label id='InformatioText'>影片價格:</label><input id='InformationInput' name='price' type='text' value='{$video['價格']}'><br>
                                <label id='InformatioText'>影片網址:</label><input id='InformationInput' name='videoHref' type='text' value='{$video['影片']}'><br>
                                <label id='InformatioText'>縮圖網址:</label><input id='InformationInput' name='imgHref' type='text' value='{$video['圖片']}'><br>
                                <label id='InformatioText'>新增日期:</label><input id='InformationInput' name='addTime' type='text' value='{$video['新增時間']}'><br>
                                <label id='InformatioText'>瀏覽次數:</label><input id='InformationInput' name='watchCount' type='text' value='{$video['瀏覽次數']}'><br>
                                <label id='InformatioText'>發行商:</label><input id='InformationInput' name='videoProducer' type='text' value='{$video['片商']}'><br>
                                <label id='InformatioText'>類別:</label><input id='InformationInput' name='tag' type='text' value='{$video['類別']}'><br>
                                <label id='InformatioText'>會員限定:</label><input id='InformationInput' name='member' type='text' value='{$video['會員限定']}'><br><br>
                                <label id='InformatioBtn'><a class='Sp' href='OP.php?VideoIndex={$video['編號']}'>修改</label>
                                <label id='InformatioBtn'><a class='Sp' href='OP.php?DelVideoIndex={$video['編號']}'>刪除</label>
                            </form>
                        </div>";
                ?>
                
        </div>
    </div>
</body>
</html>