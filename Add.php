<?php
    include("MySQL.php");
    session_start();
    $memberSql = "SELECT * FROM member WHERE 編號 = {$_SESSION['UserNum']}";
    $result = mysqli_query($link, $memberSql);
    $member = mysqli_fetch_array($result);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="Edit.css">
<!--The following script tag downloads a font from the Adobe Edge Web Fonts server for use within the web page. We recommend that you do not modify it.--><script>var __adobewebfontsappname__="dreamweaver"</script><script src="http://use.edgefonts.net/cabin-sketch:n7:default;cuprum:n4:default.js" type="text/javascript"></script>
</head>
<body id="backgroundSetting" background="img/backgroundFinal - 複製.png">
    <div class="TopFloat">
        <div class="TopFloat-Item01"><a href="Menu.php">CORNHUB</a></div>
        <div class="TopFloat-Item02">
            <form method="POST" action="SearchResult.php">
                <input type="text" name="search" id="SearchText" placeholder="搜尋影片">
                <input type="submit" id="SearchBtn" value="⊙搜尋>">
            </form>
        </div>
        <div class="TopFloat-Item04">
            <div>
                <?php
                    echo "<span id='UserName'>使用者:</span>";
                    echo "<span id='UserName'>{$member['名稱']}</span><br>";
                    echo "<span id='UserName'>會員狀態:</span>";
                    echo "<span id='UserName'>管理者</span>";
                ?>
            </div>
        </div>
        <div class="TopFloat-Item03">
            <form action="OP.php">
                <input type="submit" id="UpgradeBtn" value=" ★ 編輯">
            </form>
        </div>
    </div>

    <div class="VideoBlock">
        <span id="Title">新增影片</span>
        <div class="VideoList">
                <?php
                    echo "<div class='Video'>
                            <form class='VideoInformation' method='POST' action='OP.php?AddVideo=1''>
                                <label id='InformatioText'>影片名稱:</label><input id='InformationInput' name='videoName' type='text'><br>
                                <label id='InformatioText'>影片價格:</label><input id='InformationInput' name='price' type='text'><br>
                                <label id='InformatioText'>影片網址:</label><input id='InformationInput' name='videoHref' type='text' ><br>
                                <label id='InformatioText'>縮圖網址:</label><input id='InformationInput' name='imgHref' type='text' ><br>
                                <label id='InformatioText'>新增日期:</label><input id='InformationInput' name='addTime' type='text' ><br>
                                <label id='InformatioText'>瀏覽次數:</label><input id='InformationInput' name='watchCount' type='text' ><br>
                                <label id='InformatioText'>發行商:</label><input id='InformationInput' name='videoProducer' type='text' ><br>
                                <label id='InformatioText'>類別:</label><input id='InformationInput' name='tag' type='text'><br>
                                <label id='InformatioText'>會員限定:</label><input id='InformationInput' name='member' type='text'><br><br>
                                <label id='InformatioBtn'><input id='SubmitButton' type='submit' value='新增'></label>
                            </form>
                        </div>";
                ?>
                
        </div>
    </div>
</body>
</html>