<?php
include("MySQL.php");
session_start();
//取得全影片
$videoIndex = $_GET['videoIndex'];
$sql = "SELECT * FROM video  WHERE 編號 = $videoIndex";
$result = mysqli_query($link, $sql);

$videoCount = 0;
$video = mysqli_fetch_array($result);

$count = $video['瀏覽次數'];
$count++;
$sqlWatchCount = "UPDATE `video` SET `瀏覽次數`='$count' WHERE 編號 = $videoIndex";
mysqli_query($link, $sqlWatchCount);

$sql = "SELECT * FROM video";
$videos = Query($link, $sql);
$randIndex =  array_rand($videos, 2);
$url1 = "http://localhost/Code/Watch.php" . "?videoIndex={$videos[$randIndex[0]]['編號']}";
$url2 = "http://localhost/Code/Watch.php" . "?videoIndex={$videos[$randIndex[1]]['編號']}";


$memberSql = "SELECT * FROM member WHERE 編號 = {$_SESSION['UserNum']}";
$result = mysqli_query($link, $memberSql);
$member = mysqli_fetch_array($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="Watch.css">
    <!--The following script tag downloads a font from the Adobe Edge Web Fonts server for use within the web page. We recommend that you do not modify it.-->
    <script>
        var __adobewebfontsappname__ = "dreamweaver"
    </script>
    <script src="http://use.edgefonts.net/crimson-text:n7:default;cuprum:n4:default.js" type="text/javascript"></script>
</head>

<body>

    <body id="backgroundSetting" background="img/backgroundFinal - 複製.png">
        <div class="TopFloat">
            <div class="TopFloat-Item01"><a href="Menu.php">CORNHUB</a></div>
            <div class="TopFloat-Item02">
                <form method="POST" action="SearchResult.php">
                    <input type="text" name="search" id="SearchText" placeholder="搜尋影片">
                    <input type="submit" id="SearchBtn" value="⊙搜尋>">
                </form>
            </div>
            <div class="TopFloat-Item04">
                <div>
                    <?php
                    echo "<span id='UserName'>使用者:</span>";
                    echo "<span id='UserName'>{$member['名稱']}</span><br>";

                    echo "<span id='UserName'>會員狀態:</span>";
                    if ($member['資格'] == 1)
                        echo "<span id='UserName'>有</span>";
                    else if ($member['資格'] == 0)
                        echo "<span id='UserName'>無</span>";
                    else if (($member['資格'] == 2))
                        echo "<span id='UserName'>管理者</span>";
                    ?>
                </div>
            </div>
            <div class="TopFloat-Item03">
                <?php
                if ($member['資格'] == 0) {
                    echo "<form action='Premium.php'>";
                    echo "<input type='submit' id='UpgradeBtn' value=' ★ 升級'>";
                    echo "</form>";
                }

                if ($member['資格'] == 2) {
                    echo "<form action='Add.php'>";
                    echo "<input type='submit' id='UpgradeBtn' value=' ◹ 上傳'>";
                    echo "</form>";
                    echo "<form action='OP.php'>";
                    echo "<input type='submit' id='UpgradeBtn' value=' ★ 編輯'>";
                    echo "</form>";
                }
                ?>
            </div>
        </div>

        <div class="VideoBlock">
            <div class="LeftVideo">
                <div class="Video">
                    <?php echo "<iframe id='WatchVideo' src= {$video['影片']}  frameborder=0 scrolling=no allowfullscreen></iframe> <br/>"; ?>
                    <div class="VideoInformation">
                        <span id="VideoTitle"><?php echo "{$video['名稱']}"; ?></span><br>
                        <div class="VideoDateView">
                            <span id="VideoSubtitle"><?php echo "新增時間：{$video['新增時間']} - "; ?></span>
                            <span id="VideoSubtitle"><?php echo "瀏覽次數：{$video['瀏覽次數']}"; ?></span>
                        </div>
                    </div>
                </div>
                <div class="VideoDetail">
                    <span><?php echo "片商：{$video['片商']}" ?></span><br>
                    <span><?php echo "類別：{$video['類別']}" ?></span>
                </div>
            </div>
            <div class="RightVideo">
                <div class="FloatAD"><img id="FloatAD" src="img/TestImg.jpg"></div>
                <div class="RecommendVideo">
                    <div id="RecommendTitle">推薦影片</div>
                    <figure>
                        <?php
                        echo "<a href=$url1><img id='VideoImg' src={$videos[$randIndex[0]]['圖片']} width=213 height=120 }></a>";
                        echo "<figcaption>{$videos[$randIndex[0]]['名稱']}</figcaption>";
                        ?>
                    </figure>
                    <figure>
                        <?php
                        echo "<a href=$url2><img id='VideoImg' src={$videos[$randIndex[1]]['圖片']} width=213 height=120 }></a>";
                        echo "<figcaption>{$videos[$randIndex[1]]['名稱']}</figcaption>";
                        ?>
                    </figure>

                </div>

            </div>
        </div>

        </div>
    </body>

</html>