<?php
//資料庫設定
$dbServer = "localhost";
$dbName = "videosearcher";
$dbUser = "root";
$dbPass = "";

//連線資料庫伺服器
if (!$link = mysqli_connect($dbServer, $dbUser, $dbPass))
    die("無法連線資料庫伺服器");

//選擇資料庫
if (!mysqli_select_db($link, $dbName))
    die("無法使用資料庫");

function Query($link, $sql)
{
    $result = mysqli_query($link, $sql);

    if (mysqli_num_rows($result) > 0) {

        $vidsCount = 0;
        if ($result) {
            while ($row = mysqli_fetch_array($result)) {
                $vids[$vidsCount] = $row;
                $vidsCount++;
            }

            return $vids;
        } else {
            return "";
        }
    }
};
