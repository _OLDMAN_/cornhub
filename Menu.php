<?php
include("MySQL.php");
session_start();
$baseWatchUrl = "http://localhost/Code/Watch.php";
$basePremiumUrl = "http://localhost/Code/Premium.php";

$newSql = "SELECT * FROM video ORDER BY 新增時間 DESC";
$newVideos = Query($link, $newSql);

$mostViewSql = "SELECT * FROM video ORDER BY 瀏覽次數 DESC";
$mostViewVideos = Query($link, $mostViewSql);

$tagSql = "SELECT * FROM tag";
$tags = Query($link, $tagSql);

$memberSql = "SELECT * FROM member WHERE 編號 = {$_SESSION['UserNum']}";
$result = mysqli_query($link, $memberSql);
$member = mysqli_fetch_array($result);
// print_r("<pre>");
// print_r($member);
// print_r("</pre>");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="Menu.css">
    <!--The following script tag downloads a font from the Adobe Edge Web Fonts server for use within the web page. We recommend that you do not modify it.-->
    <script>
        var __adobewebfontsappname__ = "dreamweaver"
    </script>
    <script src="http://use.edgefonts.net/germania-one:n4:default;cuprum:n4:default.js" type="text/javascript"></script>

</head>

<body>

    <body id="backgroundSetting" background="img/backgroundFinal - 複製.png">
        <div class="TopFloat">
            <div class="TopFloat-Item01"><a href="Menu.php">CORNHUB</a></div>
            <div class="TopFloat-Item02">
                <form method="POST" action="SearchResult.php">
                    <input type="text" name="search" id="SearchText" placeholder="搜尋影片">
                    <input type="submit" id="SearchBtn" value="⊙搜尋>">
                </form>
            </div>
            <div class="TopFloat-Item04">
                <div>
                    <?php
                    echo "<span id='UserName'>使用者:</span>";
                    echo "<span id='UserName'>{$member['名稱']}</span><br>";

                    echo "<span id='UserName'>會員狀態:</span>";
                    if ($member['資格'] == 1)
                        echo "<span id='UserName'>有</span>";
                    else if($member['資格'] == 0)
                        echo "<span id='UserName'>無</span>";
                    else if(($member['資格'] == 2))
                        echo "<span id='UserName'>管理者</span>";
                    ?>
                </div>
            </div>
            <div class="TopFloat-Item03">
                <?php
                    if ($member['資格'] == 0)
                    {
                        echo "<form action='Premium.php'>";
                        echo "<input type='submit' id='UpgradeBtn' value=' ★ 升級'>";
                        echo "</form>";
                    }

                    if($member['資格']==2)
                    {
                        echo "<form action='Add.php'>";
                        echo "<input type='submit' id='UpgradeBtn' value=' ◹ 上傳'>";
                        echo "</form>";
                        echo "<form action='OP.php'>";
                        echo "<input type='submit' id='UpgradeBtn' value=' ★ 編輯'>";
                        echo "</form>";
                    }
                ?>
            </div>
        </div>

        <div class="Search">
            <div class="MainTag">
                <input type="button" id="MainTag" value="所有影片">
                <input type="button" id="MainTag" value="會員影片">
            </div>
            <form class="TagForm" method="POST" action="SearchResult.php">
                <div class="Tag">
                    <table>
                        <?php
                        $allTagCount = 10;
                        $r = 7;
                        $c = 2;
                        for ($j = 0; $j < $c; $j++) {
                            echo "<tr>";
                            for ($i = 0; $i < $r; $i++) {
                                $tagIndex = $r * $j + $i;
                                if ($tagIndex >= $allTagCount)
                                    break;
                                echo "<td>
                                        <input type='radio' id='{$tags[$tagIndex]['名稱']}' name='tag' value={$tags[$tagIndex]['名稱']}>
                                        <label for='{$tags[$tagIndex]['名稱']}'>{$tags[$tagIndex]['名稱']}</label>
                                    </td>";
                            }
                            echo "</tr>";
                        }
                        ?>
                    </table>
                </div>
                <div class="SearchTag">
                    <input type="text" name="search" id="TagSearchText" placeholder="搜尋影片">
                    <input type="submit" id="TagSearchBtn" value="搜尋">
                </div>
            </form>
        </div>

        <div class="HotVideo">
            <span id="Title">熱門影片</span>
            <div class="HotVideoList">
                <?php
                echo "<table>";
                echo "<tr>";
                for ($i = 0; $i < 5; $i++) {
                    if ($mostViewVideos[$i]['會員限定'] == "1" and $member['資格'] == "0") {
                        $url = $basePremiumUrl;
                    } else {
                        $url = $baseWatchUrl . "?videoIndex={$mostViewVideos[$i]['編號']}";
                    }
                    echo "<td><figure>";
                    echo "<a href=$url><img id='VideoImg' src={$mostViewVideos[$i]['圖片']} width=356 height=201 }></a>";
                    echo "<figcaption>{$mostViewVideos[$i]['名稱']}</figcaption>";
                    echo "</figure></td>";
                }
                echo "</tr>";
                echo "</table>";
                ?>
            </div>
        </div>

        <div class="NewVideo">
            <span id="Title">最新影片</span>
            <div class="NewVideoList">
                <?php
                $row = 5;
                $col = count($newVideos) / $row;
                if (count($newVideos) % $row != 0)
                    $col++;
                echo "<table>";
                for ($j = 0; $j < $col; $j++) {
                    echo "<tr>";
                    for ($i = 0; $i < $row; $i++) {

                        $count = $j * $row + $i;
                        if ($count >= count($newVideos))
                            break;

                        if ($newVideos[$count]['會員限定'] == "1" and $member['資格'] == "0") {
                            $url = $basePremiumUrl;
                        } else {
                            $url = $baseWatchUrl . "?videoIndex={$newVideos[$count]['編號']}";
                        }

                        echo "<td><figure><a href=$url><img id='VideoImg' src={$newVideos[$count]['圖片']} width=356 height=201 }></a>";
                        echo "<figcaption>{$newVideos[$count]['名稱']}</figcaption></figure></td>";
                    }
                }
                echo "</tr>";

                echo "</table>";
                ?>
            </div>
        </div>

        <div class="Foot">
            CORNHUB Co., Ltd.
        </div>
    </body>

</html>